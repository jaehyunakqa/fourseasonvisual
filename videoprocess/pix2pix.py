#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
@author: memo

Main app
"""

from __future__ import print_function
from __future__ import division

import numpy as np
import time

#import params
#import gui

import msa.utils
from msa.capturer import Capturer
from msa.predictor import Predictor
from msa.framestats import FrameStats

from moviepy.video.io import ffmpeg_writer
import cv2
import os

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--model_path", help="model")
parser.add_argument("--video_path", help="video")
parser.add_argument("--output_path", default='drive/My Drive/output/video/', help="video")
parser.add_argument("--dim", default = 256, help="size of output images")
parser.add_argument("--fps",  help="fps")
arg = parser.parse_args()
#%%
capture = None # msa.capturer.Capturer, video capture wrapper
predictor = None # msa.predictor.Predictor, model for prediction

img_cap = np.empty([]) # captured image before processing
img_in = np.empty([]) # processed capture image
img_out = np.empty([]) # output from prediction model


#%% init gui and params

#gui.init_app()

#pyqt_params = gui.init_params(params.params_list, target_obj=params, w=320)

# reading & writing to pyqtgraph.parametertree seems to be slow,
# so going to cache in an object for direct access
#gui.params_to_obj(pyqt_params, target_obj=params, create_missing=True, verbose=True)



#%%

# load predictor model
predictor = Predictor(arg.model_path, arg.dim)

# add video path

device_id = arg.video_path
sleep_s = 0.001
width = 680
height = 480
fps = 30
use_thread  = True
# init capture device
def init_capture(capture, output_shape):
    if capture:
        capture.close()

    capture_shape = (height, width)
    capture = Capturer(sleep_s = sleep_s,
                       device_id = device_id,
                       capture_shape = capture_shape,
                       capture_fps = fps,
                       output_shape = output_shape
                       )

    capture.update()

    if use_thread:
        capture.start()

    return capture


capture = init_capture(capture, output_shape=predictor.input_shape)

if os.path.exists(arg.output_path) == False:
    os.makedirs(arg.output_path)


if arg.fps is not None:
    fps= arg.fps
else:
    fps = capture.fps

width = arg.dim #params.Capture.Init.width
height = arg.dim #params.Capture.Init.height
title = device_id
title = title.split("/")[-1]
save_name = os.path.join(arg.output_path,title.split(".")[0]+"_out.mp4")
video_writer = ffmpeg_writer.FFMPEG_VideoWriter(save_name, (width,height), fps)


# keep track of frame count and frame rate
frame_stats = FrameStats('Main')

quit = False
reinitialise = False
enabled = True
verbose = False
freeze = False
pre_time_lerp = 0
post_time_lerp = 0.5
# main update loop
while not quit:

    # reinit capture device if parameters have changed
    if reinitialise:
        reinitialise = False
        capture = init_capture(capture, output_shape=predictor.input_shape)


    capture.enabled = enabled
    if enabled:
        # update capture parameters from GUI
        capture.output_shape = predictor.input_shape
        capture.verbose = verbose
        capture.freeze = freeze
        capture.sleep_s = sleep_s
        #for p in msa.utils.get_members(params.Capture.Processing):
        #    setattr(capture, p, getattr(params.Capture.Processing, p))

        # run capture if multithreading is disabled
        if use_thread == False:
            capture.update()

        img_cap = np.copy(capture.img) # create copy to avoid thread issues


    # interpolate (temporal blur) on input image
    img_in = msa.utils.np_lerp( img_in, img_cap, 1 - pre_time_lerp)

    # run prediction
    if enabled and predictor:
        img_predicted = predictor.predict(img_in)[0]
    else:
        img_predicted = capture.img0

    # interpolate (temporal blur) on output image
    img_out = msa.utils.np_lerp(img_out, img_predicted, 1 - post_time_lerp)

    # update frame states
    frame_stats.verbose = verbose
    frame_stats.update()

    try:
        img_out2 =  cv2.resize(img_out, (width, height))
        video_writer.write_frame(np.clip(img_out2*255, 0, 255).astype(np.uint8))
    except:
        print("error")

    if capture.thread_running == False:
        quit= True
    time.sleep(sleep_s)


# cleanup
capture.close()
video_writer.close()

capture = None
predictor = None

print('Finished')