#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
@author: memo

loads bunch of images from a folder (and recursively from subfolders)
preprocesses (resize or crop, canny edge detection) and saves into a new folder
"""

from __future__ import print_function
from __future__ import division

import numpy as np
import os
import cv2
import PIL.Image
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("--in_path", help="path to folder containing images")
parser.add_argument("--dim", default = 256, help="size of output images")
arg = parser.parse_args()
canny_thresh1 = 100
canny_thresh2 = 200

in_path = arg.in_path

#########################################
out_path = in_path + '_processed'

out_shape = (arg.dim, arg.dim)

if os.path.exists(out_path) == False:
    os.makedirs(out_path)

# eCryptfs file system has filename length limit of around 143 chars!
# https://unix.stackexchange.com/questions/32795/what-is-the-maximum-allowed-filename-and-folder-size-with-ecryptfs
max_fname_len = 141 # leave room for extension


def get_file_list(path, extensions=['jpg', 'jpeg', 'png']):
    '''returns a (flat) list of paths of all files of (certain types) recursively under a path'''
    paths = [os.path.join(root, name)
             for root, dirs, files in os.walk(path)
             for name in files
             if name.lower().endswith(tuple(extensions))]
    return paths


paths = get_file_list(in_path)
print('{} files found'.format(len(paths)))


for i,path in enumerate(paths):
    path_d, path_f = os.path.split(path)

    # combine path and filename to create unique new filename
    out_fname = path_d.split('/')[-1] + '_' + path_f

    # take last n characters so doesn't go over filename length limit
    out_fname = os.path.splitext(out_fname)[0][-max_fname_len+4:] + '.jpg'

    print('File {} of {}, {}'.format(i, len(paths), out_fname))
    im = PIL.Image.open(path)
    im = im.convert('RGB')
    im = im.resize(out_shape, PIL.Image.BICUBIC)

    a1 = np.array(im)
    a2 = cv2.Canny(a1, canny_thresh1, canny_thresh2)
    a2 = cv2.cvtColor(a2, cv2.COLOR_GRAY2RGB)
    a3 = np.concatenate((a1,a2), axis=1)
    im = PIL.Image.fromarray(a3)

    im.save(os.path.join(out_path, out_fname))
